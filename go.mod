module gitlab.com/gitlab-org/security-products/analyzers/report/v3

go 1.13

require (
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.22.1
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.0.0
)
